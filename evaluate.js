let results = {}

function insertionSort(arr) {
  for (let i = 1; i < arr.length; i++) {
    let currentValue = arr[i]
    let j
    for (j = i - 1; j >= 0 && arr[j] > currentValue; j--) {
      arr[j + 1] = arr[j]
    }
    arr[j + 1] = currentValue
  }
  return arr
}

function quicksort(array) {
  if (array.length <= 1) {
    return array
  }

  var pivot = array[0]

  var left = []
  var right = []

  for (var i = 1; i < array.length; i++) {
    array[i] < pivot ? left.push(array[i]) : right.push(array[i])
  }

  return quicksort(left).concat(pivot, quicksort(right))
}

// Function to measure the execution time
function measureExecutionTime(fn) {
  var startTime = performance.now()

  fn()

  var endTime = performance.now()

  var executionTime = endTime - startTime

  return executionTime
}

const evaluateExperiment = () => {
  const problemsize = document.getElementById("input-size").value

  const computerAlg1 = document.getElementById("system-alg-1-computer").checked
    ? "computer"
    : "rechenzentrum"
  const computerAlg2 = document.getElementById("system-alg-2-computer").checked
    ? "computer"
    : "rechenzentrum"

  const messgroesseAlg1 = document.getElementById("system-alg-1-zeit").checked
    ? "zeit"
    : "vergleiche"
  const messgroesseAlg2 = document.getElementById("system-alg-2-zeit").checked
    ? "zeit"
    : "vergleiche"

  console.log(
    "problemsize: ",
    problemsize,
    "; ",
    "PC1 / Pc2",
    computerAlg1,
    computerAlg2,
    "; ",
    "messgroesse 1/2",
    messgroesseAlg1,
    messgroesseAlg2
  )

  document.getElementById("res-1").innerText = " "
  document.getElementById("res-2").innerText = " "

  let arrayToSort = Array.from({ length: problemsize }, () => Math.random())
  console.log(arrayToSort[0])
  let laufzeitAlg1 = "Messung gestartet",
    laufzeitAlg2 = "Messung gestartet"

  //quicksort
  if (results["ALG1: " + problemsize + computerAlg1 + messgroesseAlg1]) {
    // die ergebnisse stimmen schon
    laufzeitAlg1 =
      results["ALG1: " + problemsize + computerAlg1 + messgroesseAlg1]
  } else {
    // wir müssen es berechnen

    if (messgroesseAlg1 === "zeit") {
      laufzeitAlg1 = measureExecutionTime(() => quicksort([...arrayToSort]))

      if (computerAlg1 === "rechenzentrum") {
        laufzeitAlg1 = laufzeitAlg1 / 10
      }

      laufzeitAlg1 += " ms"
    } else {
      laufzeitAlg1 =
        Math.round(problemsize * Math.log(problemsize)) + " Vergleiche"
    }

    results["ALG1: " + problemsize + computerAlg1 + messgroesseAlg1] =
      laufzeitAlg1
  }

  // insertion sort
  if (results["ALG2: " + problemsize + computerAlg2 + messgroesseAlg2]) {
    // die ergebnisse stimmen schon
    laufzeitAlg2 =
      results["ALG2: " + problemsize + computerAlg2 + messgroesseAlg2]
  } else {
    if (messgroesseAlg2 === "zeit") {
      laufzeitAlg2 = measureExecutionTime(() => insertionSort([...arrayToSort]))
      if (computerAlg2 === "rechenzentrum") {
        laufzeitAlg2 = laufzeitAlg2 / 10
      }
      laufzeitAlg2 += " ms"
    } else {
      laufzeitAlg2 = (problemsize * problemsize) / 4 + " Vergleiche"
    }

    results["ALG2: " + problemsize + computerAlg2 + messgroesseAlg2] =
      laufzeitAlg2
  }

  console.log(laufzeitAlg1, laufzeitAlg2)

  document.getElementById("res-1").innerText = laufzeitAlg1
  document.getElementById("res-2").innerText = laufzeitAlg2
}
