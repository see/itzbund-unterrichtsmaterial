const javaAlg1 = `public class QuickSort {
    public static void quicksort(int[] array, int left, int right) {
        if (left < right) {
            int pivotIndex = partition(array, left, right);
            
            // linkes und rechts Subarray sortieren
            quicksort(array, left, pivotIndex - 1);
            quicksort(array, pivotIndex + 1, right);
        }
    }
    
    public static int partition(int[] array, int left, int right) {
        int pivot = array[right]; // pivot wählen
        int pivotIndex = left;
        
        for (int i = left; i < right; i++) {
            if (array[i] <= pivot) {
                // Elemente kleiner gleich pivot
                // nach links tauschen
                swap(array, i, pivotIndex);
                pivotIndex++;
            }
        }
        
        // Pivotelement an finale Position bewegen
        swap(array, right, pivotIndex);
        
        return pivotIndex;
    }
    
    public static void swap(int[] array, int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}
`

const javaAlg2 = `public class InsertionSort {
    public static void insertionSort(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            int currentValue = arr[i];
            int j;
            for (j = i - 1; j >= 0 && arr[j] > currentValue; j--) {
                arr[j + 1] = arr[j];
            }
            arr[j + 1] = currentValue;
        }
    }
}
`

const javascriptAlg1 = `function quicksort(array) {
    if (array.length <= 1) {
        return array
    }
    
    var pivot = array[0]
    
    var left = []
    var right = []
    
    for (var i = 1; i < array.length; i++) {
        array[i] < pivot ? left.push(array[i]) : right.push(array[i])
    }
    
    return quicksort(left).concat(pivot, quicksort(right))
}      
`

const javascriptAlg2 = `function insertionSort(arr) {
    for (let i = 1; i < arr.length; i++) {
        let currentValue = arr[i]
        let j
        for (j = i - 1; j >= 0 && arr[j] > currentValue; j--) {
        arr[j + 1] = arr[j]
        }
        arr[j + 1] = currentValue
    }
    return arr
}   
`

const pythonAlg1 = `def quicksort(array):
    if len(array) <= 1:
        return array
    
    pivot = array[0]
    left = []
    right = []
    
    for i in range(1, len(array)):
        if array[i] < pivot:
            left.append(array[i])
        else:
            right.append(array[i])
    
    return quicksort(left) + [pivot] + quicksort(right)  
`

const pythonAlg2 = `def insertion_sort(arr):
    for i in range(1, len(arr)):
        current_value = arr[i]
        j = i - 1
        while j >= 0 and arr[j] > current_value:
            arr[j + 1] = arr[j]
            j -= 1
        arr[j + 1] = current_value
    return arr
`

const changeCode = (language) => {
  console.log(language)
  const codeArea1 = document.getElementById("codeArea-1")
  const codeArea2 = document.getElementById("codeArea-2")

  codeArea1.disabled = false
  codeArea2.disabled = false

  if (language === "java") {
    codeArea1.innerHTML = javaAlg1
    codeArea2.innerHTML = javaAlg2
  } else if (language === "javascript") {
    codeArea1.innerHTML = javascriptAlg1
    codeArea2.innerHTML = javascriptAlg2
  } else if (language === "python") {
    codeArea1.innerHTML = pythonAlg1
    codeArea2.innerHTML = pythonAlg2
  }

  codeArea1.disabled = true
  codeArea2.disabled = true

  window.decorator1.update()
  window.decorator2.update()
}
